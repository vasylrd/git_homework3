# Завдання 3
# Нехай на кожну сходинку можна стати з попередньої або переступивши через одну.
# Визначте, скількома способами можна піднятися на задану сходинку.


def get_steps_way(steps_count: int) -> int:
    if steps_count <= 2:
        return steps_count
    else:
        return get_steps_way(steps_count - 1) + get_steps_way(steps_count - 2)


if __name__ == "__main__":
    cases = ((1, 1), (2, 2), (3, 3), (4, 5), (5, 8), (6, 13), (7, 21), (8, 34), (9, 55))
    for steps, result in cases:
        func_result = get_steps_way(steps)
        assert result == func_result, f"Error got {func_result} expect {result}"
