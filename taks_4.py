# Завдання 4

# Напишіть рекурсивну функцію, яка обчислює суму натуральних чисел,
# які входять до заданого проміжку.


def get_range_sum(start: int, end: int) -> int:
    """Повертає суму натуральних чисел, які входять до заданого проміжку"""

    if start > end:
        return 0
    else:
        return start + get_range_sum(start + 1, end)


if __name__ == "__main__":
    print(get_range_sum(0, 10))
