# Завдання 5

# Створіть функцію quadratic_equation, яка приймає на вхід 3 параметри: a, b, c.
# Усередині цієї функції створити змінні x1, x2 зі значенням None (спочатку приймаємо, що рівняння не має коренів) та
# функцію calc_rezult з формальними параметрами зовнішньої функції quadratic_equation.
# Всередині функції calc_rezult здійснити пошук дискримінанта, згідно з результатом якого зробити
# розрахунок коренів рівняння. Зовнішня функція quadratic_equation має повернути перелік значень коренів
# квадратного рівняння.
#
# Надати можливість користувачеві ввести з клавіатури формальні параметри для передачі
# їх у створену функцію quadratic_equation, результати роботи функції відобразити на екрані.


def quadratic_equation(
    a: float, b: float, c: float
) -> tuple[float | None, float | None]:
    """Повертає кортеж значень коренів квадратного рівняння."""

    def calc_result():
        return b**2 - 4 * a * c

    x1, x2 = None, None
    discr = calc_result()
    if discr > 0:
        x1 = (-b - discr**0.5) / (2 * a)
        x2 = (-b + discr**0.5) / (2 * a)
        return x1, x2
    elif discr == 0:
        x1 = -b / (2 * a)
        return x1, x2
    else:
        return x1, x2


def is_contains_only_numbers(text: str, ls_count: int) -> bool:
    """Функція перевіряє, чи текст це список чисел із заданою довжиною"""
    text_list = text.strip().split()
    if len(text_list) != ls_count:
        return False
    for val in text_list:
        try:
            float(val)
        except ValueError:
            return False
    return True


if __name__ == "__main__":
    print("Quadratic equation")
    while True:
        values_raw = input("Please enter three values ('q' for exit): ").strip().lower()
        if values_raw == "q":
            break
        if not is_contains_only_numbers(values_raw, 3):
            print("Invalid input! Three values must be entered!")
            continue

        values = tuple(float(i) for i in values_raw.split())
        print(quadratic_equation(*values))
