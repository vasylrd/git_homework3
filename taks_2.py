# Завдання 2

# Створіть програму, яка перевіряє, чи є паліндромом введена фраза.


def is_palindrome(text: str) -> bool:
    text = text.strip().lower()
    return text == text[::-1]


if __name__ == "__main__":
    for word in ("radar", "level", "madam", "радар", "дід", "баба"):
        print(word, is_palindrome(word))
